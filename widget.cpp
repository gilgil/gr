#include "widget.h"
#include "ui_widget.h"
#include <QFile>
#include <QMessageBox>
#include <QTextStream>

Widget::Widget(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::Widget)
{
	ui->setupUi(this);

	QFile file("gr.txt");
	if(!file.open(QIODevice::ReadOnly)) {
		QMessageBox::warning(this, "Error", "Can not find gr.txt");
		return;
	}
	QStringList items;
	QTextStream in(&file);
	while(!in.atEnd()) {
		QString item = in.readLine();
		items.append(item);
	}
	file.close();

	foreach(QString item, items) {
		ui->lwMember->addItem(item);
	}
}

Widget::~Widget()
{
	delete ui;
}


void Widget::on_pbRandom_clicked()
{
	int count = ui->lwMember->count();
	int r = rand();
	int select = r % count;
	ui->lwMember->setCurrentRow(select);
}

void Widget::on_pbAddRandom_clicked()
{
	int count = ui->lwMember->count();
	int add = ui->leAddRandom->text().toInt();
	int r = rand();
	int current = ui->lwMember->currentRow();
	int select = (current + (r % add) + 1) % count;
	ui->lwMember->setCurrentRow(select);
}
